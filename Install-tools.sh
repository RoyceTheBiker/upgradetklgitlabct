#!/bin/bash

for i in tools; do
	[ -f /usr/local/bin/${i} ] && {
		install -v -m755 -oroot -groot ${i} /usr/local/bin/${i}
	} || true
done