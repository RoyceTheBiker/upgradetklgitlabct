mv config.toml{,back}
sed -i /home/git/gitlab/config/gitlab.yml -e '/gitaly:$/a\ \ \ \ client_path: \/home\/git\/gitaly\/bin'
cd /home/git/gitlab
#echo "Pausing for sleep 10000"
#sleep 10000 || true

sed -i /home/git/gitlab/config/environments/production.rb -e "/^end/i\ \ config.secret_key_base = ENV['SECRET_KEY_BASE']"

sudo -u git -H bundle install --without postgres development test --deployment
#echo "Pausing for sleep 20000"
#sleep 10000 || true
sudo -u git -H RAILS_ENV=production bundle exec rake "gitlab:gitaly:install[/home/git/gitaly]"
#echo "Pausing for sleep 30000"
#sleep 10000 || true
cd /home/git/gitaly
