[ -S /var/run/redis/redis.sock ] || {
	# If you don't want to use a socket on the local redis, to F'in bad! GitLab is going to demand that it be there.
	# sed -i /etc/redis/unixsocket.conf -e '/^unixsocket/d'
	sed -i /etc/redis/redis.conf -e 's|.*unixsocket.*var.run.redis.*|unixsocket /var/run/redis/redis.sock|'
	service redis-server restart
}

