#!/bin/bash

# https://tweenpath.net/permanent/useful-proxmox-lxc-commands/


# usage 
# cat Proxmox_Restore_Test_CT.sh | ssh root@192.168.0.47
# ./PushStart.sh root@192.168.0.77

pct stop 108

# The CT should be off but the C-Pannel needs time to update.
sleep 10

# GitLab 8.17, Debian 9, Ruby 2.3
BF=/var/lib/vz/dump/vzdump-lxc-108-2018_12_11-16_17_14.tar.gz

[ -f $BF ] || {
	# Original GitLab 8.17 clone. Debian 8, Ruby 2.1
	BF=/var/lib/vz/dump/vzdump-lxc-108-2018_12_10-11_37_24.tar.gz
}

pct restore 108 ${BF} --force true --storage local-zfs

[ -f ~/GitLab_CT.my.cnf ] && {
	CTD=$(pct config 108|grep ^rootfs|sed -e 's/.*:\(.*\),.*/\1/')
	cp -v ~/GitLab_CT.my.cnf /rpool/data/${CTD}/root/.my.cnf
}

# After upgrading to GitLab 10 there was very little free disk space in the container. 
# Increase disk size by 2GB, from 8GB to 10GB
pct resize 108 rootfs +2G

# The container ran out of memory trying to migrate the DB. My GitLab only has four projects and 2GB of memory.
# If a GitLab has more projects and a larger DB, it will need more memory.
# https://pve.proxmox.com/pve-docs/pct.1.html
RAM=$((4 * 1024))
(( $(pct config 108|grep memory|awk '{print $NF}') < $RAM )) && {
	pct set 108 -memory $RAM
}


pct start 108

# Give time for the CT to startup
sleep 30

