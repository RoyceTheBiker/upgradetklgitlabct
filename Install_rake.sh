#!/bin/bash

set -e

# The TurnkeyLinux GitLab 14 did not come with rake installed.
gem install rake

# https://gitlab.com/gitlab-org/gitlab-ce/commit/1d5e5f754df345d7e3748bef4030b7210cff63c9

cd /home/git/gitlab

cat > Gemfile.patch <<@EOF
--- Gemfile.lock        2017-05-19 04:31:23.000000000 +0000
+++ Gemfile.fixed       2018-12-11 23:55:56.395827529 +0000
@@ -467,11 +467,10 @@
       omniauth (~> 1.0)
       omniauth-oauth2 (~> 1.0)
     omniauth-google-oauth2 (0.4.1)
-      addressable (~> 2.3)
-      jwt (~> 1.0)
+      jwt (~> 1.5.2)
       multi_json (~> 1.3)
       omniauth (>= 1.1.1)
-      omniauth-oauth2 (~> 1.3.1)
+      omniauth-oauth2 (>= 1.3.1)
     omniauth-kerberos (0.3.0)
       omniauth-multipassword
       timfel-krb5-auth (~> 0.8)
@@ -1011,4 +1010,4 @@
   wikicloth (= 0.8.1)

 BUNDLED WITH
-   1.14.5
+   1.15.0
@EOF

patch Gemfile.lock Gemfile.patch

gem install bundler
bundle install


sudo -u git -H git checkout -- Gemfile.lock

