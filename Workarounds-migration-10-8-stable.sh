cd /home/git/gitlab
git checkout HEAD Gemfile
git checkout HEAD Gemfile.lock

#sed -i config/database.yml -e 's/encoding:.*/encoding: utf8mb4/'
#sed -i config/database.yml -e '/encoding/a\ \ collation: utf8mb4_general_ci'

sudo -u git -H bundle exec rake add_limits_mysql RAILS_ENV=production

mysql -e "SET storage_engine=INNODB;"
mysql -e "SET GLOBAL innodb_file_per_table=1, innodb_file_format=Barracuda, innodb_large_prefix=1;"
mysql -e "SET GLOBAL log_bin_trust_function_creators = 1;"
# mysql -e 'CREATE DATABASE IF NOT EXISTS `gitlabhq_production` DEFAULT CHARACTER SET `utf8` COLLATE `utf8_general_ci`;'
mysql -e "SET GLOBAL log_bin_trust_function_creators = 1;"
grep log_bin_trust_function_creators /etc/mysql/mariadb.conf.d/50-server.cnf -q && {
	sed -i /etc/mysql/mariadb.conf.d/50-server.cnf -e 's/.*log_bin_trust_function_creators.*/log_bin_trust_function_creators=1/'
} || {
	TH=$(mktemp /dev/shm/TH_XXXXXXXXXXXXXXXXXXXXXXXXX)
	echo "innodb_default_row_format=dynamic
innodb_file_format=barracuda
innodb_file_per_table=true
innodb_large_prefix=true
log_bin_trust_function_creators=1" >> $TH
	sed -i /etc/mysql/mariadb.conf.d/50-server.cnf -e "/^\[*mysqld*\]/r${TH}"
	rm -f $TH
	service mysql restart
}
#mysql -e 'GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, CREATE TEMPORARY TABLES, DROP, INDEX, ALTER, LOCK TABLES, REFERENCES, TRIGGER ON `gitlabhq_production`.* TO "gitlab"@"localhost";'

# sudo -u git -H bundle exec rake gitlab:gitaly:install RAILS_ENV=production

cat lib/support/init.d/gitlab > /etc/init.d/gitlab