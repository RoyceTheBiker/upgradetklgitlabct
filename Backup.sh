# Backup
# As this is a CT a backup is not needed because Proxmox is managing imaging.
cd /home/git/gitlab
sudo -u git -H mkdir -pv /home/git/gitlab/public/uploads
sudo -u git -H bundle exec rake gitlab:backup:create RAILS_ENV=production
