#!/bin/bash

SetGlv "9-5-stable"

set -e

cd /home/git
chown git:git ./ -R || true

Source StopServices.sh

Source Backup.sh

Source GitLab.sh

Source GitLab-Shell.sh

Source GitLab-Workhorse.sh


Source Gitaly.sh


# Update MySQL permissions. Gitlab.com documented the MySQL user as git but TKL has it as gitlab
mysql -e "GRANT TRIGGER ON \`gitlabhq_production\`.* TO 'gitlab'@'localhost';"

# Workaround for SHOW FULL FIELDS FROM
# This is okay here because it is a TKL CT and the local DB only servs GitLab. 
# Would not want to do this on a DB that is shared with other services.
mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'gitlab'@'localhost';"

Source DbMigration.sh

Source StartServices.sh

Source StatusReport.sh
