#!/bin/bash

SetGlv "10-8-stable"

set -e

cd /home/git
chown git:git ./ -R || true

Source StopServices.sh

Source Backup.sh

rm -f package.json yarn.lock
Source GitLab.sh

Source GitLab-Shell.sh

Source GitLab-Workhorse.sh


Source Gitaly.sh


# Update MySQL permissions. Gitlab.com documented the MySQL user as git but TKL has it as gitlab
mysql -e "GRANT TRIGGER ON \`gitlabhq_production\`.* TO 'gitlab'@'localhost';"

mysql -e "SET GLOBAL log_bin_trust_function_creators = 1;"

Source DbMigration.sh

Source StartServices.sh

Source StatusReport.sh
