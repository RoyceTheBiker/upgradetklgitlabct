#!/bin/bash

echo "$0 $@"

ORIG_DIR=$PWD
# This opens a split screen to show progress in one pannel and continue the rest of the Setup in the other pannel
[ "${1}x" = "x" ] && {
	echo "About to split screen"
	# yum -y install screen
	screen -c screen.conf
}

[ "${1}x" = "continuex" ] && {
	cat Setup.sh | sed -ne '/after split.screen.sh/,$p' | /bin/bash
	killall tail
}

echo "Done"
sleep 10