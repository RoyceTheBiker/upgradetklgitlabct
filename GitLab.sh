cd /home/git/gitlab
sudo -u git -H git fetch --all
sudo -u git -H git checkout -- db/schema.rb
[ -f $ORIG_DIR/GitLab-$GLV.sh ] && source $ORIG_DIR/GitLab-$GLV.sh || true
sudo -u git -H git checkout -- app/views/layouts/application.html.haml
sudo -u git -H git checkout -- db/fixtures/production/001_admin.rb
sudo -u git -H git checkout ${GLV}
