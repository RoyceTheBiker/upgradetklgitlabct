# Install libs, migrations, etc.
cd /home/git/gitlab

# MySQL installations (note: the line below states '--without postgres')
sudo -u git -H bundle install --without postgres development test --deployment

# Optional: clean up old gems
sudo -u git -H bundle clean

# Run database migrations
[ -f $ORIG_DIR/Workarounds-migration-$GLV.sh ] && source $ORIG_DIR/Workarounds-migration-$GLV.sh || true
sudo -u git -H bundle exec rake db:migrate RAILS_ENV=production

[ -f $ORIG_DIR/Workarounds-gettext-$GLV.sh ] && source $ORIG_DIR/Workarounds-gettext-$GLV.sh || true


# Update node dependencies and recompile assets
sudo -u git -H bundle exec rake yarn:install gitlab:assets:clean gitlab:assets:compile RAILS_ENV=production NODE_ENV=production NO_SOURCEMAPS=true

# Clean up cache
sudo -u git -H bundle exec rake cache:clear RAILS_ENV=production
