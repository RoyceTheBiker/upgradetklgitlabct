#!/bin/bash

set -e

function GitLab11 {
	dpkg -r ruby2.3
	mkdir /tmp/ruby && cd /tmp/ruby
	curl --remote-name --progress https://cache.ruby-lang.org/pub/ruby/2.5/ruby-2.5.3.tar.gz
	echo 'f919a9fbcdb7abecd887157b49833663c5c15fda  ruby-2.5.3.tar.gz' | shasum -c - && tar xzf ruby-2.5.3.tar.gz
	cd ruby-2.5.3
	./configure --disable-install-rdoc
	make
	make install
}

function OlderGitLab {
	dpkg -r ruby2.1
	apt-get -y autoremove

	PACKAGE_RUBY=1
	((PACKAGE_RUBY == 1)) && {
		apt-get -y install ruby2.3
	} || {
		mkdir /tmp/ruby && cd /tmp/ruby
		curl --remote-name --progress https://cache.ruby-lang.org/pub/ruby/2.3/ruby-2.3.3.tar.gz
		echo '1014ee699071aa2ddd501907d18cbe15399c997d ruby-2.3.3.tar.gz' | shasum -c - && tar xzf ruby-2.3.3.tar.gz
		cd ruby-2.3.3
		./configure --disable-install-rdoc
		make
		sudo make install
	}
}

function SelectRubyToInstall {
	[[ $GLV == "11-"* ]] && {
		GitLab11
		return
	}
	OlderGitLab 
}

SelectRubyToInstall

