#!/bin/bash

# 11-6-stable is not available yet
# git remote update; git ls-remote | grep "[0-9]-stable$"
SetGlv "11-4-stable"

set -e

cd /home/git
chown git:git ./ -R || true

Source Backup.sh

Source StopServices.sh

Source Upgrade_go_language.sh
rm -f package.json yarn.lock
Source GitLab.sh

Source GitLab-Shell.sh

Source GitLab-Workhorse.sh

Source Gitaly.sh

Source GitLab-Pages.sh

# Many need updates to gitlab/config/unicorn.rb
# https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/update/11.5-to-11.6.md

Source DbMigration.sh

Source StartServices.sh

Source StatusReport.sh
