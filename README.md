This is my adventure to upgrade my GitLab. Originally installed as a TurnKey Linux image running in my Proxmox container. It came with GitLab 8-17-stable.

If you wish to run my scripts to update your GitLab, consider that it could fail. I recommend using some type of VM so the system can be cloned and rebuilt if it fails.


Proxmox_Restore_Test_CT.sh restores the backup I took of the container.

PushStart.sh SSH's into my container and starts the upgrade.

Setup.sh is the script that downloads the new version of GitLab and performs the commands to upgrade it.

The upgrade is done in steps.
1) Upgrade 8-17-stable to 9-0-stable
2) Upgrade 9-0-stable to 9-5-stable
3) Upgrade 9-5-stable to 10-8-stable
4) Upgrade 10-8-stable to 11-4.0-rc6
