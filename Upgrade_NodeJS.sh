#!/bin/bash

set -e

# This is not needed. The node version installed in Deb9 is 6.15.1 but dpkg reports the package as 4.8.2-dfsg-1

function NodeJS6FromSource {	
	# https://nodejs.org/dist/latest-v6.x/
	F=node-v6.15.1.tar.gz
	curl --remote-name --progress https://nodejs.org/dist/latest-v6.x/${F}
	echo '3e08c82c95ab32f476199369e894b48d70cbaaaa12c1b67f60584c618a6eb0ca  ${F}' | sha256sum -c - && tar xzf ${F}
	cd node-v6.15.1
	./configure
	make
	make install
}

function InstallNodeJS {
	[[ $GLV == "11-"* ]] && {
		NodeJS6
		return
	}
}

function InstallNvm {
TMP=$(mktemp /dev/shm/HERE_SCRIPT_XXXXXXXXXXXXX)

cat > $TMP << EOF
#!/bin/bash

sed -i .bashrc -e '/NVM_DIR/d'
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.4/install.sh > NvmInstaller.sh
bash ./NvmInstaller.sh
sed -i ~/.bashrc -e "s|^export NVM_DIR.*|export NVM_DIR=\"\${PWD}/.nvm\"|"
source ~/.bashrc

# This installs the latest version.
# nvm install node 

nvm install $NODEJS_VER

nvm run node --version

npm install pm2 -g

EOF

chmod 644 $TMP

}

## Installing from nvm
# NODEJS_VER=6
# InstallNvm


## Installing from source
# mkdir /tmp/NodeJS6
# cd /tmp/NodeJS6
# InstallNodeJS
# cd
# rm -rf /tmp/NodeJS6


