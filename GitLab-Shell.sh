cd /home/git/gitlab-shell
sudo -u git -H git fetch --all --tags
sudo -u git -H git checkout v$(</home/git/gitlab/GITLAB_SHELL_VERSION)
[ -f ./bin/compile ] && {
	sudo -u git -H bin/compile
} || { 
	echo "Compile is not ready yet."
}
