#!/bin/bash

SetGlv "9-0-stable"

set -e

cd /home/git
chown git:git ./ -R || true

Source StopServices.sh

Source RedisSocket.sh

Source Backup.sh

Source InstallBundler.sh


# Update Node
apt-get -y install nodejs

# Update yarn
apt-get -y install yarn


Source GitLab.sh

Source GitLab-Shell.sh

Source GitLab-Workhorse.sh


# Update configuration files
# Not sure

# Git configuration
cd /home/git/gitlab
sudo -u git -H git config --global repack.writeBitmaps true

# Nginx configuration
# not sure


Source Gitaly.sh

apt-get -y install libre2-dev

Source DbMigration.sh

Source StartServices.sh

Source StatusReport.sh

