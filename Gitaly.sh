[ -d /home/git/gitaly ] || {
	# This shit never works
	## https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/install/installation.md#install-gitaly
	## Fetch Gitaly Source with Git and compile with Go
	##sudo -u git -H bundle exec rake "gitlab:gitaly:install[/home/git/gitaly,/home/git/repositories]" RAILS_ENV=production

	# Check it out manually
	cd /home/git
	sudo -u git -H git clone https://gitlab.com/gitlab-org/gitaly.git

	## If you are using non-default settings you need to update config.toml

echo '
[gitaly-ruby]
dir = "/home/git/gitaly/ruby"

[gitlab-shell]
dir = "/home/git/gitlab-shell"
' | sudo -u git tee -a /home/git/gitaly/config.toml


	# https://gitlab.com/gitlab-org/gitlab-development-kit/issues/262
	grep gitaly /home/git/gitlab/config/gitlab.yml -q || {
cat >> /home/git/gitlab/config/gitlab.yml <<@EOF
  gitaly:
    enabled: true

  repositories:
    storages: # You must have at least a `default` storage path.
      default:
        path: /home/git/repositories/
        gitaly_address: unix:/home/git/gitlab/tmp/sockets/private/gitaly.socket

@EOF

	}

}


[ -d /home/git/gitlab/tmp/sockets/private ] && {
	# Restrict Gitaly socket access
	sudo chmod 0700 /home/git/gitlab/tmp/sockets/private
	sudo chown git /home/git/gitlab/tmp/sockets/private
} || {
	echo "Skip chmod & chown on /home/git/gitlab/tmp/sockets/private for now."
}

# Due to a bug in the rake gitlab:gitaly:install script your Gitaly configuration file may contain syntax errors. The block name
# [[storages]], which may occur more than once in your config.toml file, should be [[storage]] instead.

cd /home/git/gitaly
sudo -u git -H git fetch --all --tags
sudo -u git -H git checkout v$(</home/git/gitlab/GITALY_SERVER_VERSION)
sudo -u git -H make
[ -f $ORIG_DIR/Workarounds-gitaly-$GLV.sh ] && source $ORIG_DIR/Workarounds-gitaly-$GLV.sh || true
