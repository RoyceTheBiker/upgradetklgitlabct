#!/bin/bash

# usage 
# cat Proxmox_Restore_Test_CT.sh | ssh root@192.168.0.47
# ./PushStart.sh root@192.168.0.77

echo "
echo export COLUMNS=$(tput cols) > tty.info
echo export LINES=$(tput lines) >> tty.info
#stty cols $COLUMNS
#stty rows $LINES
script install.log

cd GitLab.BigHouse/Systems/GitLab.TurnkeyLinux/
git pull
git reset --hard origin/master

./Setup.sh

echo Review the install.log" | ssh $1

