#!/bin/bash

set -e

function InstallGo1_10 {
	curl --remote-name --progress https://dl.google.com/go/go1.10.5.linux-amd64.tar.gz
	echo 'a035d9beda8341b645d3f45a1b620cf2d8fb0c5eb409be36b389c0fd384ecc3a  go1.10.5.linux-amd64.tar.gz' | shasum -a256 -c - && {
		rm -rf /usr/local/go
		tar -C /usr/local -xzf go1.10.5.linux-amd64.tar.gz
		sudo ln -sf /usr/local/go/bin/{go,godoc,gofmt} /usr/local/bin/
		rm go1.10.5.linux-amd64.tar.gz
	}
}

function InstallGo {
	[[ $GLV == "11-"* ]] && {
		InstallGo1_10
		return
	}
}

InstallGo
go version

