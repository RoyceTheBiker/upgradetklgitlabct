[ -d /home/git/gitlab-pages ] || {
	# Check it out manually
	cd /home/git
	sudo -u git -H git clone https://gitlab.com/gitlab-org/gitlab-pages.git
}

cd /home/git/gitlab-pages
sudo -u git -H git fetch --all --tags
sudo -u git -H git checkout v$(</home/git/gitlab/GITLAB_PAGES_VERSION)
sudo -u git -H make
[ -f $ORIG_DIR/Workarounds-gitlab-pages-$GLV.sh ] && source $ORIG_DIR/Workarounds-gitlab-pages-$GLV.sh || true