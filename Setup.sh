#!/bin/bash

source ~/tty.info
export TERM="xterm"

# TKL official upgrade documentation for older versions.
# https://github.com/turnkeylinux/tracker/wiki/Upgrade-GitLab-in-v14.0-and-v14.1-appliances

# Important files
# DB creds
# /home/git/gitlab/config/database.yml


export GLV=

# cating the install.log is hard to read with all the charactors that screen adds
# This will strip off most of the junk
# cat install.log | sed -e "s,\x1B\[[0-9;]*[a-zA-Z],,g"
export CRASH_REPORT=/dev/shm/progress.log
date > $CRASH_REPORT

echo "colorscheme torte" > ~/.vimrc

which screen && source split.screen.sh || true
exit # screen runs everything after this line so we stop here
# Report extra details to the user by appending text to progress.log
# after split.screen.sh starts, it will run everything below this point.


exec 19> $CRASH_REPORT
BASH_XTRACEFD="19"

CrashReported() {
	(($? != 0)) && {
		[[ $TERM == "screen" ]] && sleep 84000 || cat $CRASH_REPORT | sed -ne '1,/CrashReported/p'
		[ -f /root/GLV ] && {
			source /root/GLV
			echo "While trying to install ${GLV}"
		}
		echo "PWD=$PWD"
		sleep 360
	}
	# rm -f $CRASH_REPORT
}
trap "CrashReported" EXIT

set -ex

export ORIG_DIR=$PWD

# This CT does not have an IPv6 device, when Deb9 is intalled networking fails to start
sed -i /etc/network/interfaces -e '/inet6/d'

# apt-get -y install vim

SetGlv() {
	GLV=$1
	echo "GLV=$1" > /root/GLV
}

Source() {
	# No crash yet, truncate the crash report
	echo "--------------------------------" >> $CRASH_REPORT
	echo "     $1 $GLV" >> $CRASH_REPORT
	cd $ORIG_DIR
	source $1
}

# Stop GitLab before upgrading Ruby or the service will not be able to stop gracefully
Source StopServices.sh

[ -f /root/STEP1 ] || {
	Source ./DebUpgrade_Jessie_to_Stretch.sh
	Source ./Upgrade_ruby.sh
	Source ./Install_yarn.sh
	Source ./Install_go_language.sh
	Source ./Upgrade_git.sh
	touch /root/STEP1
	echo "Create a backup for faster rebuilding"
	exit 0
}

Source ./Install-tools.sh
Source ./Install_rake.sh
Source ./Upgrade_GitLab-8-to-9.sh
Source ./Upgrade_GitLab-9.0-to-9.sh
Source ./Upgrade_GitLab-9.5-to-10-8.sh
Source ./Upgrade_GitLab-10.8-to-11.sh
