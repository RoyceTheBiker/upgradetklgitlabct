#!/bin/bash

set -e


# https://www.stunnel.org/static/stunnel.html
# options = NO_SSLv2
# Use sslVersionMax or sslVersionMin option instead of disabling specific TLS protocol versions when compiled with OpenSSL 1.1.0 or later.
# dpkg -r stunnel4
# sslVersionMin=TLSv1.2
# stunnel4 fails to start with "sslVersionMin = TLSv1.2": Specified option name is not valid here
# sed -i /etc/stunnel/stunnel.conf -e 's/^\(options.*NO_SSL\)/# \1/'
# sed -i /etc/stunnel/stunnel.conf -e '/# options.*NO_SSL/,/^;/{s/^$/sslVersionMin=TLSv1.2\n/}'

# TurnkeyLinux GitLab 15 has options = NO_SSLv3
sed -i /etc/stunnel/stunnel.conf -e 's/^\(options.*NO_SSLv2\)/# \1/'

apt-get -y update
apt-get -y install apt-transport-https
apt-get -y autoremove

for i in $(grep jessie /etc/apt/sources.list.d/ -lr); do
	sed -i $i -e 's/jessie/stretch/g'
done
apt-key update
apt-get -y update
OPS="--force-confnew --force-confdef"
OPS="--force-confnew"
DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::="$OPS" --force-yes -fuy -y dist-upgrade
DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::="$OPS" --force-yes -fuy -y install vim

# Postfix configuration may need review.
# /etc/ssh/sshd.config my need changes
# redis.conf my need changes
# Remove grub



# https://github.com/libgit2/libgit2/issues/3995
# Compiling rugged will fail without libssl1.0-dev installed.
apt-get -y install libssl1.0-dev
